#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/wait.h>
#include <unistd.h>

/*
 * Function declarations
 */
int do_fail(int status, char *msg);
char *read_line(void);
char **split_line(char *line);
int msh_exec(char **args);
int msh_launch(char **args);
void msh_loop(void);

/* 
 * Function declarations for builtin shell commands
 */
int msh_cd(char **args);
int msh_help(char **args);
int msh_exit(char **args);

/*
 * List of builtin commands, followed by corresponding functions
 */
char *builtin_str[] = {
	"cd",
	"help",
	"exit"
};

int (*builtin_func[]) (char **) = {
	&msh_cd,
	&msh_help,
	&msh_exit
};

/*
 * Implementation of general functionality.
 */
int do_fail(int status, char *msg) {
	fprintf(stderr, "msh: %s", msg);
	exit(status);
}

int msh_num_builtins() {
	return sizeof(builtin_str) / sizeof(char *);
}

/*
 * Builtin function implementation
 */
int msh_cd(char **args) {
	if (args[1] == NULL) {
		fprintf(stderr, "msh: expected argument to \"cd\"\n");
	} else {
		if (chdir(args[1]) != 0) {
			perror("msh");
		}
	}

	return 1;
}

int msh_help(char **args) {
	int i;
	printf("Max Resing's msh\n");
	printf("(inspired by https://brennan.io/2015/01/16/write-a-shell-in-c/\n");
	printf("Type program names and arguments.\n");
	printf("Builtins:\n");

	for(i = 0; i < msh_num_builtins(); i++) {
		printf("    %s\n", builtin_str[i]);
	}

	printf("Use the man command for information on other programs.\n");
	return 1;
}

int msh_exit(char **args) {
	return 0;
}

#define RL_BUFFER_SIZE 1024
char *read_line(void) {
	// obsolete code, because getline already does it
	/*
	int buffer_size = RL_BUFFER_SIZE;
	int position = 0;
	char *buffer = malloc(sizeof(char) * buffer_size);
	int c;

	if (!buffer)  {
		do_fail(EXIT_FAILURE, "allocation error\n");
	}

	while (1) {
		// Read a character
		c = getchar();	

		// If EOF, replace with null char and return
		if (c == EOF || c == '\n') {
			buffer[position] = '\0';
			return buffer;
		} else {
			buffer[position] = c;
		}

		position++;


		// If buffer exceeded, reallocate
		if (position >= buffer_size) {
			buffer_size += RL_BUFFER_SIZE;
			buffer = realloc(buffer, buffer_size);
			if(!buffer) {
				do_fail(EXIT_FAILURE, "allocation error\n");
			}
		}
	}
	*/

	char *line = NULL;	
	size_t buffer_size = 0; // getline allocates the buffer for us
	getline(&line, &buffer_size, stdin);
	return line;
}

#define TOKEN_BUFFER_SIZE 64
#define TOKEN_DELIMITER " \t\r\n\a"
char **split_line(char *line) {
	int buffer_size = TOKEN_BUFFER_SIZE, position = 0;
	char **tokens = malloc(sizeof(char) * buffer_size);
	char *token;

	if (!tokens) {
		do_fail(EXIT_FAILURE, "allocation_error\n");
	}

	token = strtok(line, TOKEN_DELIMITER);
	while (token != NULL) {
		tokens[position] = token;
		position++;

		if (position >= buffer_size) {
			buffer_size += TOKEN_BUFFER_SIZE;
			tokens = realloc(tokens, sizeof(char*) * buffer_size);
			if (!tokens) {
				do_fail(EXIT_FAILURE, "allocation error\n");
			}
		}

		token = strtok(NULL, TOKEN_DELIMITER);
	}

	tokens[position] = NULL;
	return tokens;
}

int msh_exec(char **args) {
	int i;

	if (args[0] == NULL) {
		// Empty command was entered
		return 1;
	}

	for (i = 0; i < msh_num_builtins(); i++) {
		if (strcmp(args[0], builtin_str[i]) == 0) {
			return (*builtin_func[i])(args);
		}
	}

	return msh_launch(args);
}

int msh_launch(char **args) {
	pid_t pid, wpid;
	int status;

	pid = fork();

	if(pid == 0) {
		// Child process
		if (execvp(args[0], args) == -1) {
			perror("msh");
		}
		exit(EXIT_FAILURE);
	} else if (pid > 0) {
		// Parent process
		do {
			wpid = waitpid(pid, &status, WUNTRACED);
		} while (!WIFEXITED(status) && !WIFSIGNALED(status));
	} else {
		// Error forking
		perror("msh");
	}

	return 1;
}

void msh_loop(void) {
	char *line;
	char **args;
	int status;

	do {
		printf("> ");
		line = read_line();
		args = split_line(line);

		status = msh_exec(args);

		free(line);
		free(args);
	} while (status);
}


int main(int argc, char **argv) {
	// load config files
	
	// run command loop
	msh_loop();

	// perform shutdown & cleanup
	
	return EXIT_SUCCESS;
}

